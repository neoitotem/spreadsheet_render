import React from 'react';
import {
  ChakraProvider,
  Box,
  Text,
  Link,
  VStack,
  HStack,
  Code,
  Grid,
  theme,
  Spacer,
  Flex,
  Heading,
  Button,
  Icon
} from '@chakra-ui/react';
import UploadModel from "./components/UploadModal"
import { ColorModeSwitcher } from './ColorModeSwitcher';
import OverviewDemo from "./components/DisplayData";

function App() {
  return (
    <ChakraProvider>
      <Flex alignItems="center" bg="purple.500">
        <Box p="4">
          <Heading size="md">Logo</Heading>
        </Box>
        <Spacer />
        <Box p="2">
          
        </Box>
      </Flex>
      <Flex p="3">
        <Spacer />
        <UploadModel />
      </Flex>
      <Flex>
        <Box>
        <OverviewDemo /> 
        </Box>
      </Flex>
    </ChakraProvider>
  );
}


export default App;
