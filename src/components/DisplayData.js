import "ka-table/style.css";
// or import "ka-table/style.scss";
import React, { useState, useContext, useEffect } from 'react';
import { kaReducer, Table } from 'ka-table';
import { DataType, EditingMode, SortingMode } from 'ka-table/enums';
import { UserContext } from "../UserContext.js";
import * as XLSX from 'xlsx';
import Papa from "papaparse";
import _ from "lodash";
import { Heading, Flex, Spacer, Box, Center, Container } from "@chakra-ui/react";


// data for ka-table
const dataArray = Array(10).fill(undefined).map(
  (_, index) => ({  
    column1: `column:1 row:${index}`,
    column2: `column:2 row:${index}`,
    column3: `column:3 row:${index}`,
    column4: `column:4 row:${index}`,
    id: index,
  }),
);
// initial value of the *props
var tablePropsInit = {
  columns: [
    { key: 'column1', title: 'Column 1', dataType: DataType.String },
    { key: 'column2', title: 'Column 2', dataType: DataType.String },
    { key: 'column3', title: 'Column 3', dataType: DataType.String },
    { key: 'column4', title: 'Column 4', dataType: DataType.String },
  ],
  data: dataArray,
  editingMode: EditingMode.Cell,
  rowKeyField: 'id',
  sortingMode: SortingMode.Single,
};

function OverviewDemo() {
  // in this case *props are stored in the state of parent component
  const [tableProps, changeTableProps] = useState(tablePropsInit);
  const [unclean, setUnclean] = useState("");
  const dataUnclean = useContext(UserContext);

  useEffect(() => {
    if(dataUnclean.data){
    fileHandler(dataUnclean.data, unclean);
    // Papa.parse(dataUnclean.data[0], {header: true,complete: (result)=>{setUnclean(result)}});
    // if(unclean[0]){
    //   const header = unclean[0].split(',')
    //   const headerlsit = header.map((head, index) => { return { key: head.toLocaleLowerCase(), title: head, dataType: DataType.String }; })
    //     tablePropsInit = {
    //       columns: headerlsit,
    //       data: arr,
    //       editingMode: EditingMode.Cell,
    //       rowKeyField: 'id',
    //       sortingMode: SortingMode.Single,
    //     }
    //   }
    }
  }, [dataUnclean]);

  function fileHandler(file, unclean){
    if(unclean === ""){
    if(file[0].name.search('xlsx')>0){
    const reader = new FileReader();
    reader.onload = (evt) => {
      /* Parse data */
      const bstr = evt.target.result;
      const wb = XLSX.read(bstr, { type: 'binary' });
      /* Get first worksheet */
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      /* Convert array of arrays */
      const data = XLSX.utils.sheet_to_json(ws, {header:1,raw:false,dateNF:'yyyy-mm-dd'});
      console.log(data);
      const parsedData = makexl(data);
      parsedData[0].pop();
      setUnclean(parsedData);
    }
      reader.readAsBinaryString(file[0])
    }
    else if (file[0].name.search('csv')>0){
      Papa.parse(file[0], {header: true,complete:(result)=>{
            for(let i=0; i<result.data.length; i++){
              result.data[i] = {...result.data[i], id: i}
            }
            var header = Object.keys(result.data[0]);
            result.data.pop();
            const compdata  = [header, result.data]
            setUnclean(compdata);
        }
      });
    }
  }
  else{
    if(file[0].name.search('xlsx')>0){
      const reader = new FileReader();
      reader.onload = (evt) => {
        /* Parse data */
        const bstr = evt.target.result;
        const wb = XLSX.read(bstr, { type: 'binary' });
        /* Get first worksheet */
        const wsname = wb.SheetNames[0];
        const ws = wb.Sheets[wsname];
        /* Convert array of arrays */
        const data = XLSX.utils.sheet_to_json(ws, {header:1,raw:false,dateNF:'yyyy-mm-dd'});
        console.log(data);
        const parsedData = makexl(data);
        console.log(parsedData);
        const correctData = headerCorrection(parsedData[0], parsedData[1])
        const headers = unclean[0].concat(correctData[0]);
        const merg = unclean[1].concat(parsedData[1]);
        const hed = [ ...new Set(headers)];
        console.log(hed);
        merg.pop();
        const compdata  = [hed, merg];
        console.log(compdata);
        setUnclean(compdata);
      }
        reader.readAsBinaryString(file[0])
      }
      else if (file[0].name.search('csv')>0){
        Papa.parse(file[0], {header: true,complete:(result)=>{
              for(let i=0; i<result.data.length; i++){
                //var id = Math.random() * 100;
                result.data[i] = {...result.data[i], id: i+unclean[1].length};
              }
              console.log(unclean);
              var header = Object.keys(result.data[0]);
              const correctData = headerCorrection(header, result.data)
              console.log(correctData);
              // for(let i=0; i<correctData[0].length;i++){
              //   if(unclean[0].includes(correctData[0][i])){
              //     const removed =correctData[0].splice(i,1);
              //     console.log(removed);
              //   }
              // }
              // for(let i=0; i<unclean[0].length;i++){
              //   console.log(unclean[0][i]);
              //   console.log(correctData[0][i]);
              //   for(let j=0; j<correctData[0].length; j++){
              //     if(unclean[0][i] === correctData[0][i]){
              //       const removed =correctData[0].splice(i,1);
              //       console.log(removed);
              //     }
              //   }
              // }
              console.log(correctData[0])
              const headers = unclean[0].concat(correctData[0]);
              const merg = unclean[1].concat(result.data);
              const hed = [ ...new Set(headers)];
              merg.pop();
              const compdata  = [hed, merg];
              setUnclean(compdata);
          }
        });
      }
  }
  }
  function makexl(xlarray){
    var id = 100;
    var Chead = xlarray[0];
    var Cbody = [];
    var temobj = new Object();
    for(let j=1; j<xlarray.length;j++){
      for(let i=0; i<Chead.length;i++){
        id++
        if(xlarray[j][i] === undefined){
          xlarray[j][i] = "";
        }
        temobj[Chead[i]] = xlarray[j][i]
      }
      Cbody.push({...temobj, id: id});
    }
    return [Chead, Cbody];
  }
  function murgerow(Cdata, Ndata){
    console.log(Cdata);
    console.log(Ndata);function murgerow(Cdata, Ndata){
      console.log(Cdata);
      console.log(Ndata);
      var complete = [];
      for(let i=0; i<Cdata.length; i++){
        for(let j=0; j<Ndata.length; j++){
          if(JSON.stringify(Cdata[i]) !== JSON.stringify(Ndata[j])){
            complete.push(Ndata[j])
            continue;
          }
        }
      }
      console.log(complete);
    }
    var complete = [];
    for(let i=0; i<Cdata.length; i++){
      for(let j=0; j<Ndata.length; j++){
        if(JSON.stringify(Cdata[i]) !== JSON.stringify(Ndata[j])){
          complete.push(Ndata[j])
          continue;
        }
      }
    }
    console.log(complete);
  }

  useEffect(() => {
    if(unclean){
      //console.log(unclean);
      // var header = Object.keys(unclean[0]);
      var header = unclean[0];
      header = header.filter((result, index)=> {return result !== "id"})
      console.log(header);
      var correctedData = "";
      // console.log(header);
      correctedData = headerCorrection(header, unclean[1]);
      console.log(unclean);
      console.log(correctedData);
      const headerlist = header.map((result, index)=>{ return {key: result, title: result ,dataType: DataType.String}})
      
      tablePropsInit = {
          columns: headerlist,
          data: correctedData[1],
          editingMode: EditingMode.Cell,
          rowKeyField: 'id',
          sortingMode: SortingMode.Single,
        }
      }
    changeTableProps(tablePropsInit);
  }, [unclean])

function headerCorrection(Cheader,Cbody){
  const dictionary = [
    { value: 'Dato',
      toChange: ['Validato', 'Valutadato', 'Date']
     },
     {
       value: 'itiscorrect',
       toChange: ['itsnot', 'noway', 'changeitbro']
     },
     {
      value: 'herewegoagain',
      toChange: ['changeme', 'notthistime', 'comeon']
    }
  ];
  // for(var m=0; m<dictionary.length; m++){
  //   for(var i=0; i< Cheader.length; i++){
  //     for(var j=0; j< dictionary[m].toChange.length; j++){
  //       if(Cheader[i] === dictionary[m].toChange[j]){
  //         for(var k=0; k<Cbody.length; k++){
  //           Cbody[k][dictionary[m].value]=Cbody[k][Cheader[i]];
  //           delete Cbody[k][Cheader[i]];
  //         }
  //         Cheader[i] = dictionary[m].value;
  //       }
  //     }
  //   }
  // }
    // for (let m = 0; m < Cheader.length; m++) {
    //   for (let i = 0; i < dictionary.length; i++) {
    //     if (dictionary[i].toChange.includes(Cheader[m])) {
    //       Cheader[m] = dictionary[i].value
    //     }
    //   }
    // }
    for (let m = 0; m < Cheader.length; m++) {
      for (let i = 0; i < dictionary.length; i++) {
        if (dictionary[i].toChange.includes(Cheader[m])) {
          for (let j = 0; j < Cbody.length; j++) {
            Cbody[j][dictionary[i].value] = Cbody[j][Cheader[m]]
          }
          Cheader[m] = dictionary[i].value;
          break;
        }
      }
    }
  return [Cheader, Cbody];
}


  const dispatch = (action) => { // dispatch has an *action as an argument
    // *kaReducer returns new *props according to previous state and *action, and saves new props to the state
    changeTableProps((prevState) => kaReducer(prevState, action));
  };

  if(dataUnclean.data){
      return (
        <Table
        {...tableProps} // ka-table UI is rendered according to props
        dispatch={dispatch} // dispatch is required for obtain new actions from the UI
        />
      );
    }
  else{
    return(
      <Container maxW="3xl" maxH="3xl" centerContent>
        <Box padding="4" maxW="3xl">
          <Heading as="h6" size="lg">Please Upload Files !</Heading>    
        </Box>
    </Container>
    );
  }
};

export default OverviewDemo;