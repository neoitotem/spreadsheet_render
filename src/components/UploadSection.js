import { Button } from "@chakra-ui/react";
import React, { useState, useContext } from "react";
import { FileDrop } from 'react-file-drop';
import "./Uploadsection.css"
import  { UserContext } from '../UserContext.js';

function UploadSection(){
    const user = useContext(UserContext);
    const styles = { border: '2px dashed gray', color: 'white', padding: 20 };
    return (
      <div>
        <div style={styles}>
          <FileDrop
            // onFrameDragEnter={(event) => console.log('onFrameDragEnter', event)}
            // onFrameDragLeave={(event) => console.log('onFrameDragLeave', event)}
            // onFrameDrop={(event) => console.log('onFrameDrop', event)}
            // onDragOver={(event) => console.log('onDragOver', event)}
            // onDragLeave={(event) => console.log('onDragLeave', event)}
            onDrop={(files, event) => {
              user.setData(files);
            }}
          >
            Drop some files here!
          </FileDrop>
        </div>
      </div>
    );
}

export default UploadSection;