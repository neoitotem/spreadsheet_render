import React from "react";
import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
    Button,
    Icon,
    Text,
    useDisclosure
  } from "@chakra-ui/react";
  import UploadSection from "./UploadSection";
  import { MdFileUpload } from "react-icons/md";    

  function BasicUsage() {
    const { isOpen, onOpen, onClose } = useDisclosure()
    return (
      <>
        <Button onClick={onOpen}>
          <Icon as={MdFileUpload} /> <Text p="2">Upload</Text>
        </Button>
        <Modal isOpen={isOpen} onClose={onClose}>
          <ModalOverlay />
          <ModalContent>
            <ModalCloseButton />
            <ModalHeader>
                Upload Your File Here
            </ModalHeader>
            <ModalBody>
                <UploadSection />
            </ModalBody>
            <ModalFooter>
              <Button colorScheme="blue" mr={3} onClick={onClose}>
                Close
              </Button>
              <Button variant="ghost">Upload</Button>
            </ModalFooter>
          </ModalContent>
        </Modal>
      </>
    )
  }
export default BasicUsage;