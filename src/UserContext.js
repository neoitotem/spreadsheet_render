import React, { createContext, useState } from "react";

export const UserContext = createContext();

export const UserProvider = ({ children }) => {
    const [data, setData] = useState("");
    const [event, setEvent] = useState("")

    return(
        <UserContext.Provider
            value={{
                data,
                event,
                setData,
                setEvent
            }}>
                {children}
            </UserContext.Provider>
    );
};