import { ColorModeScript } from '@chakra-ui/react';
import React, { StrictMode } from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import * as serviceWorker from './serviceWorker';
import { UserProvider } from "./UserContext.js";

ReactDOM.render(
  <UserProvider>
  <StrictMode>
    <ColorModeScript />
    <App />
  </StrictMode>
  </UserProvider>,
  document.getElementById('root')
);
